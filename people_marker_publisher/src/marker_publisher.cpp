#include <ros/ros.h>
#include <ros/console.h>
#include <visualization_msgs/Marker.h>
#include "std_msgs/Float32MultiArray.h"
#include <vector>
#include <string>
geometry_msgs::Point P;
geometry_msgs::Point P1;
std::vector <visualization_msgs::Marker> vectorLines;

std::vector<geometry_msgs::Point> vectorP;
visualization_msgs::Marker centroid;

visualization_msgs::Marker createLine( int id_marker, geometry_msgs::Point firstPoint, geometry_msgs::Point secondPoint )
{
    
    visualization_msgs::Marker line_strip;
    line_strip.pose.orientation.w =1.0;
    line_strip.header.frame_id = "/kinect2_ir_optical_frame";
    line_strip.header.stamp = ros::Time::now();
    line_strip.ns = "points_and_lines"+ std::to_string(id_marker);
    line_strip.id = id_marker;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_strip.scale.x = 0.03;
    line_strip.color.b = 1.0;
    line_strip.color.a = 1.0;
    line_strip.lifetime = ros::Duration(0.01);
    line_strip.points.push_back(firstPoint);
    line_strip.points.push_back(secondPoint);
    // marker_pub.publish(line_strip);
    return line_strip;
}

visualization_msgs::Marker createMultiLine( int id_marker, std::vector<geometry_msgs::Point> firstPoint )
{
    visualization_msgs::Marker line_strip;
    line_strip.pose.orientation.w =1.0;
    line_strip.header.frame_id = "/kinect2_ir_optical_frame";
    line_strip.header.stamp = ros::Time::now();
    line_strip.ns = "points_and_lines"+ std::to_string(id_marker);
    line_strip.lifetime = ros::Duration(0.500);
    line_strip.id = id_marker;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_strip.scale.x = 0.03;
    line_strip.color.b = 1.0;
    line_strip.color.a = 1.0;
    for (auto& point: firstPoint)
        line_strip.points.push_back(point);
    // line_strip.points.push_back(secondPoint);
    // marker_pub.publish(line_strip);
    return line_strip;
}

void people_cb_centroids(const std_msgs::Float32MultiArray msg)
{
    // ROS_WARN_STREAM("Inside callback");
    if (msg.data.size() == 0)
    {
        return;
    }
    if (msg.data.size() % 6 == 0)
    {
        // ROS_WARN_STREAM("(msg.data.size() % 6 == 0)");
        centroid.pose.orientation.w =1.0;
        centroid.header.frame_id = "/kinect2_ir_optical_frame";
        centroid.header.stamp = ros::Time::now();
        centroid.ns = "centroid";
        centroid.lifetime = ros::Duration(1.000);
        centroid.id = 0;
        centroid.type = visualization_msgs::Marker::LINE_STRIP;
        centroid.scale.x = 0.03;
        centroid.color.b = 1.0;
        centroid.color.a = 1.0;
        geometry_msgs::Point Point;
        Point.x = msg.data[0];
        Point.y = msg.data[1];
        Point.z = msg.data[2];
        geometry_msgs::Point Point1;
        centroid.points.clear();
        centroid.points.push_back(Point);
        Point1.x = msg.data[3];
        Point1.y = msg.data[4];
        Point1.z = msg.data[5];
        centroid.points.push_back(Point1);
        // line_strip.points.push_back(secondPoint);
        // marker_pub.publish(line_strip);
    
    }
}

void people_cb(const std_msgs::Float32MultiArray msg)
{
    // for(auto & point : msg.data)
    //     ROS_WARN_STREAM( msg.data.size());
    int lines_counter = 0;
    vectorLines.clear();
    vectorP.clear();
    if (msg.data.size() % 6 == 0)
    {   for (int i=0; i < msg.data.size(); i+=6)
        {
            P.x = msg.data[i];
            P.y = msg.data[i+1];
            P.z = msg.data[i+2];

            vectorP.push_back(P);            
            P.x = msg.data[i];
            P.y = msg.data[i+4];
            P.z = msg.data[i+2];

            vectorP.push_back(P);            
            P.x = msg.data[i+3];
            P.y = msg.data[i+4];
            P.z = msg.data[i+2];

            vectorP.push_back(P);            

            P.x = msg.data[i+3];
            P.y = msg.data[i+1];
            P.z = msg.data[i+2];

            vectorP.push_back(P);            

            P.x = msg.data[i+0];
            P.y = msg.data[i+1];
            P.z = msg.data[i+2];

            vectorP.push_back(P);            

  
            vectorLines.push_back(createMultiLine(lines_counter++,vectorP));    
            vectorP.clear();      
        }

        
    }
    else 
    {
        ROS_WARN_STREAM( "Unexpected Len of message");
    }
}

int main( int argc, char** argv )
{
    ros::init(argc, argv, "listener");
    ros::NodeHandle n;
    ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);
    ros::Publisher marker_pub_centr = n.advertise<visualization_msgs::Marker>("visualization_marker_centroids", 1);
    ros::Subscriber subs_people_detected = n.subscribe<std_msgs::Float32MultiArray >("/detected_people_positions",0 , people_cb);
    ros::Subscriber subs_people_detected_centroid = n.subscribe<std_msgs::Float32MultiArray >("/detected_people_centroids",0 , people_cb_centroids);
    uint32_t shape = visualization_msgs::Marker::CUBE;
    ros::Rate r(10); // 10 hz
    float f = 0;

    while (ros::ok())
    {
        

        if (vectorLines.size() > 0)
        {
            for (auto& line : vectorLines)
                marker_pub.publish(line);
        }
        if (centroid.header.frame_id == "/kinect2_ir_optical_frame")
        marker_pub_centr.publish(centroid);
        ros::spinOnce();
        r.sleep();
        
    }
}