#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <turtlesim/Pose.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/static_transform_broadcaster.h>


std::string turtle_name;



void sendTransform(){

  static tf2_ros::StaticTransformBroadcaster static_broadcaster;
  static tf2_ros::TransformBroadcaster unstatic_broadcaster;
  geometry_msgs::TransformStamped static_transformStamped;

  static_transformStamped.header.stamp = ros::Time::now();
  static_transformStamped.header.frame_id = "world";
  static_transformStamped.child_frame_id = "kinect2_ir_optical_frame";
  static_transformStamped.transform.translation.x = 0;
  static_transformStamped.transform.translation.y = 0;
  static_transformStamped.transform.translation.z = 1.4;
  
  tf2::Quaternion quat;
  quat.setRPY(1.57/1.3,-1.57*2,0);
  static_transformStamped.transform.rotation.x = quat.x();
  static_transformStamped.transform.rotation.y = quat.y();
  static_transformStamped.transform.rotation.z = quat.z();
  static_transformStamped.transform.rotation.w = quat.w();

  static_broadcaster.sendTransform(static_transformStamped);
  unstatic_broadcaster.sendTransform(static_transformStamped);
  static_transformStamped.header.stamp = ros::Time::now();
  static_transformStamped.header.frame_id = "kinect2_ir_optical_frame";
  static_transformStamped.child_frame_id = "base_link";
  static_transformStamped.transform.translation.x = -0.45;
  static_transformStamped.transform.translation.y = 0.35;
  static_transformStamped.transform.translation.z = 2.4;
  
  
  quat.setRPY(2,0,0);
  static_transformStamped.transform.rotation.x = quat.x();
  static_transformStamped.transform.rotation.y = quat.y();
  static_transformStamped.transform.rotation.z = quat.z();
  static_transformStamped.transform.rotation.w = quat.w();

  static_broadcaster.sendTransform(static_transformStamped);
  unstatic_broadcaster.sendTransform(static_transformStamped);




//   static tf::TransformBroadcaster br;
//   tf::Transform transform;
//   transform.setOrigin( tf::Vector3(0, 1, 2.0) );
//   tf::Quaternion q;
//   q.setRPY(0, 0, 1.57);
//   transform.setRotation(q);
//   br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "kinect2_ir_optical_frame", "base_link"));


  
  
//   transform.setOrigin( tf::Vector3(0, 0, 0.0) );
  
//   q.setRPY(0, 0, 0);
//   transform.setRotation(q);
//   br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world","kinect2_ir_optical_frame"));
}

int main(int argc, char** argv){
    
    ros::init(argc, argv, "my_tf_broadcaster");  
    ros::NodeHandle node;
    ros::Rate r(10);
    while (ros::ok())
    {
        sendTransform();
        
        ros::spinOnce();
        r.sleep();
    }

    // ros::spin();
    return 0;
};